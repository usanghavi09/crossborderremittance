
function getOption() {
    selectElement = document.querySelector('#select1');
    output = selectElement.value;
    document.querySelector('.output').textContent = output;
}

function login(event){
    
    var user=document.getElementById("userType").value;
    const readBankId = document.getElementById('name1').value;
    
    if(user == "RBI"){
       
       // localStorage.setItem("aa","abc");
       //alert("Rbi login")
       window.location.href = "/rbi";
       alert(user)
       // buttonDisStatus();  
    }else if(user == "BANK"){
        
        var data=fetchBankData(event);
      
        if(data != null){
            if(data.bankId == readBankId){
                localStorage.setItem("bank",readBankId);
                window.location.href = "/bank";
            }else{
                alert("Bank is not registerd");
            }
            
        }else{
            alert("Bank is not registerd");
        }

        // localStorage.setItem("aa","abc");
        //alert("Bank login")
        
        // buttonDisStatus();  
    }else{
        alert("Please Select a User type");
    }
}

function rbiLogin(){
    window.location.href = "/rbi"
}

function logout(){
    localStorage.setItem("aa","def");
    buttonDisStatus(); 
}

function buttonDisStatus(){
    
    // var loginStatus=localStorage.getItem("aa");
    // var votingStatus=localStorage.getItem("votingStatus");
    
    //if(loginStatus == "abc"){

//     document.getElementById("login").style.display="none";
//     document.getElementById("logout").style.display="block"; 

//     }else{

//         document.getElementById("login").style.display="block";
//         document.getElementById("logout").style.display="none";
       
//     }
}



function addData(event) {
    event.preventDefault();
    console.log("addData Function")
    const bankId = document.getElementById('bankId').value;
    const bankName = document.getElementById('bankName').value;
    const bankCurrency = document.getElementById('bankCurrency').value;
    const bankCountry = document.getElementById('bankCountry').value;

   
    console.log(bankId );
    if (bankId.length == 0 ) {
        alert("Please enter the data properly");

    }
    else {
        fetch('/createBank', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
                BankId: bankId,
                BankName :bankName,
                BankCountry:bankCountry,
                BankCurrency :bankCurrency,
                Amount:0
            })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Added a new bank");

            } else {
                alert("Error in processing request");
            }

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    }

}

function readData(event) {

    event.preventDefault();
    const readBankId = document.getElementById('name1').value;

    console.log(readBankId);

    if (readBankId.length == 0) {
        alert("Please enter the data properly");
    }
    else {
        fetch('/readBank', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ BankId: readBankId })
        })
            .then(function (response) {
                console.log(response);
                if (response.status != 200) {
                    console.log(response.status)
                    // alert("Error in processing request");

                } else {
                    return response.json();
                }
            })
            .then(function (Bankdata) {
                dataBuf = Bankdata["Bankdata"]
                localStorage.setItem("bankData",dataBuf);
                console.log(dataBuf)
                //alert(dataBuf);
            })
            .catch(function (err) {
                console.log(err);
                alert("Bank is not registered");
            })
    }
}

function latestData(event) {

    event.preventDefault();
    const readBankId = localStorage.getItem("bank")

    console.log(readBankId);

    if (readBankId.length == 0) {
        alert("Please enter the data properly");
    }
    else {
        fetch('/readBank', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ BankId: readBankId })
        })
            .then(function (response) {
                console.log(response);
                if (response.status != 200) {
                    console.log(response.status)
                    // alert("Error in processing request");

                } else {
                    return response.json();
                }
            })
            .then(function (Bankdata) {
                dataBuf = Bankdata["Bankdata"]
                localStorage.setItem("bankData",dataBuf);
                console.log(dataBuf)
                //alert(dataBuf);
            })
            .catch(function (err) {
                console.log(err);
                alert("Bank is not registered");
            })
    }
}
function fetchLatestData(event){
    latestData(event);
   var bankData=localStorage.getItem("bankData");
   bankData=JSON.parse(bankData);
 
   return bankData;
}


function fetchBankData(event){

   readData(event);
   var bankData=localStorage.getItem("bankData");
   bankData=JSON.parse(bankData);
 
   return bankData;
}


function updateAmount(event) {
    event.preventDefault();
    console.log("addData Function");
    const issuerBankId =localStorage.getItem("bank");
    const acquirerBankId = document.getElementById('bankId1').value;
    const fees = document.getElementById('fees').value;
    const TxnType = "Debit";
    const amount = document.getElementById('amount').value;
    const excRate=document.getElementById('rate').value;


   
    //console.log(BankId +fees+TxnType+amount);
    if (acquirerBankId.length == 0 ) {
        alert("Please enter the data properly");

    }
    else {
        fetch('/updateAmount', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
                issuerBankId: issuerBankId,
                acquirerBankId:acquirerBankId,
                Fees :fees,
                TxnType:TxnType,
                amount:amount,
                excRate:excRate
            })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Money sent successfully");

            } else {
                alert("Error in processing request");
            }

        }).then(function (response) {
            addRevenue(issuerBankId,acquirerBankId,fees)

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    }

}

function addAmount(event) {
    event.preventDefault();
    console.log("addData Function")
    //const BankId = document.getElementById('bankId2').value;
    const issuerBankId =localStorage.getItem("bank");
    const acquirerBankId="none";
    const fees = 0;
    const TxnType = "Credit";
    const amount = document.getElementById('amount2').value;
    const excRate=0;


   
    //console.log(BankId +fees+TxnType+amount);
    if (issuerBankId.length == 0 ) {
        alert("Please enter the data properly");

    }
    else {
        fetch('/updateAmount', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
                issuerBankId: issuerBankId,
                acquirerBankId:acquirerBankId,
                Fees :fees,
                TxnType:TxnType,
                amount:amount,
                excRate:excRate
            })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Amount addedd successfully");

            } else {
                alert("Error in processing request");
            }

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    }

}


function getAllBanks(){

   
            fetch('/allBank', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ })
            })
                .then(function (response) {
                    console.log(response);
                    if (response.status != 200) {
                        console.log(response.status)
                        // alert("Error in processing request");
    
                    } else {
                        return response.json();
                    }
                })
                .then(function (Bankdata) {
                    dataBuf = Bankdata["Bankdata"]
                    localStorage.setItem("allBanks",dataBuf);
                    console.log(dataBuf)
                    //alert(dataBuf);
                })
                .catch(function (err) {
                    console.log(err);
                    alert("Error in processing request");
                })


}


function fetchAllBanks(){

    getAllBanks();

    var allBanks=localStorage.getItem("allBanks");
    allBanks=JSON.parse(allBanks);
    return(allBanks);
}

function getBankHistory(bankId){


    if (bankId.length == 0) {
        alert("Please enter the data properly");
    }
    else {
        fetch('/bankHistory', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ BankId: bankId })
        })
            .then(function (response) {
                console.log(response);
                if (response.status != 200) {
                    console.log(response.status)
                    // alert("Error in processing request");

                } else {
                    return response.json();
                }
            })
            .then(function (Bankdata) {
                dataBuf = Bankdata["Bankdata"]
                localStorage.setItem("bankHistory",dataBuf);
                console.log(dataBuf)
                //alert(dataBuf);
            })
            .catch(function (err) {
                console.log(err);
                alert("Bank is not registered");
            })
    }
}

function fetchBankHistory(){
    var bankId =localStorage.getItem("bank");

    getBankHistory(bankId)

    var bankHistory=localStorage.getItem("bankHistory");
     bankHistory=JSON.parse(bankHistory);
    return(bankHistory);
}

function addRevenue(issuerBankId,acquirerBankId,Fees) {
   
   
        fetch('/addRevenu', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ 
                issuerBankId: issuerBankId,
                acquirerBankId :acquirerBankId,
                Fees:Fees
            })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Revenue");

            } else {
                alert("Error in processing request");
            }

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    

}

function getReveHistory(){


  
        fetch('/revHistory', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({  })
        })
            .then(function (response) {
                console.log(response);
                if (response.status != 200) {
                    console.log(response.status)
                    // alert("Error in processing request");

                } else {
                    return response.json();
                }
            })
            .then(function (Bankdata) {
                dataBuf = Bankdata["Bankdata"]
                localStorage.setItem("revHistory",dataBuf);
                console.log(dataBuf)
                //alert(dataBuf);
            })
            .catch(function (err) {
                console.log(err);
                alert("Error");
            })
    
}

function fetchRevHistory(){
    
    getReveHistory();

    var revHistory=localStorage.getItem("revHistory");
    revHistory=JSON.parse(revHistory);
    return(revHistory);
}