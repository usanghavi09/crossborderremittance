/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const crypto = require('crypto');

async function getCollectionName(ctx) {
    const mspid = ctx.clientIdentity.getMSPID();
    const collectionName = `_implicit_org_${mspid}`;
    return collectionName;
}

class ExchangeContract extends Contract {

    async exchangeExists(ctx, exchangeId) {
        const collectionName = await getCollectionName(ctx);
        const data = await ctx.stub.getPrivateDataHash(collectionName, exchangeId);
        return (!!data && data.length > 0);
    }

    async createExchange(ctx, exchangeId ,currency ,rate) {
        const exists = await this.exchangeExists(ctx, exchangeId);
        if (exists) {
            throw new Error(`The asset exchange ${exchangeId} already exists`);
        }

        const privateAsset = {};

        const transientData = ctx.stub.getTransient();
        if (transientData.size === 0 || !transientData.has('privateValue')) {
            throw new Error('The privateValue key was not specified in transient data. Please try again.');
        }
        // privateAsset.privateValue = transientData.get('privateValue').toString();
        privateAsset.currencyId = transientData.get('exchangeId').toString();
        privateAsset.currencyName = transientData.get('currency').toString();
        privateAsset.currencyRate = transientData.get('rate').toString();
        privateAsset.assetType = 'private'


        const collectionName = await getCollectionName(ctx);
        await ctx.stub.putPrivateData(collectionName, exchangeId, Buffer.from(JSON.stringify(privateAsset)));
    }

    async readExchange(ctx, exchangeId) {
        const exists = await this.exchangeExists(ctx, exchangeId);
        if (!exists) {
            throw new Error(`The asset exchange ${exchangeId} does not exist`);
        }
        let privateDataString;
        const collectionName = await getCollectionName(ctx);
        const privateData = await ctx.stub.getPrivateData(collectionName, exchangeId);
        privateDataString = JSON.parse(privateData.toString());
        return privateDataString;
    }

    async updateExchange(ctx, exchangeId) {
        const exists = await this.exchangeExists(ctx, exchangeId);
        if (!exists) {
            throw new Error(`The asset exchange ${exchangeId} does not exist`);
        }
        const privateAsset = {};

        const transientData = ctx.stub.getTransient();
        if (transientData.size === 0 || !transientData.has('privateValue')) {
            throw new Error('The privateValue key was not specified in transient data. Please try again.');
        }
        privateAsset.privateValue = transientData.get('privateValue').toString();

        const collectionName = await getCollectionName(ctx);
        await ctx.stub.putPrivateData(collectionName, exchangeId, Buffer.from(JSON.stringify(privateAsset)));
    }

    async deleteExchange(ctx, exchangeId) {
        const exists = await this.exchangeExists(ctx, exchangeId);
        if (!exists) {
            throw new Error(`The asset exchange ${exchangeId} does not exist`);
        }
        const collectionName = await getCollectionName(ctx);
        await ctx.stub.deletePrivateData(collectionName, exchangeId);
    }

    async verifyExchange(ctx, mspid, exchangeId, objectToVerify) {

        // Convert provided object into a hash
        const hashToVerify = crypto.createHash('sha256').update(objectToVerify).digest('hex');
        const pdHashBytes = await ctx.stub.getPrivateDataHash(`_implicit_org_${mspid}`, exchangeId);
        if (pdHashBytes.length === 0) {
            throw new Error('No private data hash with the key: ' + exchangeId);
        }

        const actualHash = Buffer.from(pdHashBytes).toString('hex');

        // Compare the hash calculated (from object provided) and the hash stored on public ledger
        if (hashToVerify === actualHash) {
            return true;
        } else {
            return false;
        }
    }


}

module.exports = ExchangeContract;
