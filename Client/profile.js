let profile={
    rbi:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/rbi.cbr.com",
        "CP":"../Network/vars/profiles/cbrchannel_connection_for_nodesdk.json"
    },
    bank:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/bank.cbr.com",
        "CP":"../Network/vars/profiles/cbrchannel_connection_for_nodesdk.json"        
    },
    exchange:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/exchange.cbr.com",
        "CP":"../Network/vars/profiles/cbrchannel_connection_for_nodesdk.json"        
    },
}
module.exports={profile}