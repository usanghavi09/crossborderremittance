/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { RbiContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('RbiContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new RbiContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"rbi 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"rbi 1002 value"}'));
    });

    describe('#rbiExists', () => {

        it('should return true for a rbi', async () => {
            await contract.rbiExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a rbi that does not exist', async () => {
            await contract.rbiExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createRbi', () => {

        it('should create a rbi', async () => {
            await contract.createRbi(ctx, '1003', 'rbi 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"rbi 1003 value"}'));
        });

        it('should throw an error for a rbi that already exists', async () => {
            await contract.createRbi(ctx, '1001', 'myvalue').should.be.rejectedWith(/The rbi 1001 already exists/);
        });

    });

    describe('#readRbi', () => {

        it('should return a rbi', async () => {
            await contract.readRbi(ctx, '1001').should.eventually.deep.equal({ value: 'rbi 1001 value' });
        });

        it('should throw an error for a rbi that does not exist', async () => {
            await contract.readRbi(ctx, '1003').should.be.rejectedWith(/The rbi 1003 does not exist/);
        });

    });

    describe('#updateRbi', () => {

        it('should update a rbi', async () => {
            await contract.updateRbi(ctx, '1001', 'rbi 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"rbi 1001 new value"}'));
        });

        it('should throw an error for a rbi that does not exist', async () => {
            await contract.updateRbi(ctx, '1003', 'rbi 1003 new value').should.be.rejectedWith(/The rbi 1003 does not exist/);
        });

    });

    describe('#deleteRbi', () => {

        it('should delete a rbi', async () => {
            await contract.deleteRbi(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a rbi that does not exist', async () => {
            await contract.deleteRbi(ctx, '1003').should.be.rejectedWith(/The rbi 1003 does not exist/);
        });

    });

});
