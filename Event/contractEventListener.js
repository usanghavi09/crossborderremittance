const { EventListener } = require('./events')

let RbiEvent = new EventListener();

RbiEvent.contractEventListener("Rbi", "Admin", "cbrchannel",
    "KBA-CrossBorderRemmitance", "BankContract", "addBank");


RbiEvent.contractEventListener("Rbi", "Admin", "cbrchannel",
    "KBA-CrossBorderRemmitance", "RbiContract", "addRevenue");

