/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { BankContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('BankContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new BankContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"bank 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"bank 1002 value"}'));
    });

    describe('#bankExists', () => {

        it('should return true for a bank', async () => {
            await contract.bankExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a bank that does not exist', async () => {
            await contract.bankExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createBank', () => {

        it('should create a bank', async () => {
            await contract.createBank(ctx, '1003', 'bank 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"bank 1003 value"}'));
        });

        it('should throw an error for a bank that already exists', async () => {
            await contract.createBank(ctx, '1001', 'myvalue').should.be.rejectedWith(/The bank 1001 already exists/);
        });

    });

    describe('#readBank', () => {

        it('should return a bank', async () => {
            await contract.readBank(ctx, '1001').should.eventually.deep.equal({ value: 'bank 1001 value' });
        });

        it('should throw an error for a bank that does not exist', async () => {
            await contract.readBank(ctx, '1003').should.be.rejectedWith(/The bank 1003 does not exist/);
        });

    });

    describe('#updateBank', () => {

        it('should update a bank', async () => {
            await contract.updateBank(ctx, '1001', 'bank 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"bank 1001 new value"}'));
        });

        it('should throw an error for a bank that does not exist', async () => {
            await contract.updateBank(ctx, '1003', 'bank 1003 new value').should.be.rejectedWith(/The bank 1003 does not exist/);
        });

    });

    describe('#deleteBank', () => {

        it('should delete a bank', async () => {
            await contract.deleteBank(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a bank that does not exist', async () => {
            await contract.deleteBank(ctx, '1003').should.be.rejectedWith(/The bank 1003 does not exist/);
        });

    });

});
