function queryBank(event){
    document.getElementById("queryBank").style.display="block";
    document.getElementById("addamt").style.display="none";
    document.getElementById("sendMoney").style.display="none";
    document.getElementById("bankHistory").style.display="none";
    displayBank(event);
}

function displayBank(event){
    
 var data=fetchLatestData(event);
//  data=JSON.parse(data);
const container = document.createElement('div');

for (const key in data) {
  const paragraph = document.createElement('h3');
  const text = document.createTextNode(`${key}: ${data[key]}`);
  paragraph.appendChild(text);
  container.appendChild(paragraph);
}
// Append table to the document body
document.getElementById("bankInfo").innerHTML="";
document.getElementById("bankInfo").appendChild(container);
}

function addamt(){
    document.getElementById("queryBank").style.display="none";
    document.getElementById("addamt").style.display="block";
    document.getElementById("sendMoney").style.display="none";
    document.getElementById("bankHistory").style.display="none";
}

function sendMoney(){
    document.getElementById("queryBank").style.display="none";
    document.getElementById("addamt").style.display="none";
    document.getElementById("sendMoney").style.display="block";
    document.getElementById("bankHistory").style.display="none";
}


function displayBankHistory(){

    document.getElementById("queryBank").style.display="none";
    document.getElementById("addamt").style.display="none";
    document.getElementById("sendMoney").style.display="none";
    document.getElementById("bankHistory").style.display="block";

    var data= fetchBankHistory();

    var table = document.createElement("table");
    table.className="myTable"

// Create the table headers
var thead = document.createElement("thead");
var headerRow = document.createElement("tr");
for (var key in data[0]) {
  var th = document.createElement("th");
  th.textContent = key;
  headerRow.appendChild(th);
}
thead.appendChild(headerRow);
table.appendChild(thead);

// Create the table body
var tbody = document.createElement("tbody");
for (var i = 0; i < data.length; i++) {
  var rowData = data[i];
  var row = document.createElement("tr");
  for (var key in rowData) {
    var cell = document.createElement("td");
    if (typeof rowData[key] === "object") {
      cell.textContent = JSON.stringify(rowData[key]);
    } else {
      cell.textContent = rowData[key];
    }
    row.appendChild(cell);
  }
  tbody.appendChild(row);
}
table.appendChild(tbody);

// Add the table to the document body
document.getElementById("bankHistory").innerHTML="";
document.getElementById("bankHistory").appendChild(table);

}