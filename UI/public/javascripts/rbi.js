function registerBank(){
    document.getElementById("registerBank").style.display="block";
    document.getElementById("registerCurrency").style.display="none";
    document.getElementById("allCurrency").style.display="none";
    document.getElementById("allBank").style.display="none";
    document.getElementById("revHistory").style.display="none";
}

function registerCurrencyForm(){
    document.getElementById("registerBank").style.display="none";
    document.getElementById("registerCurrency").style.display="block";
    document.getElementById("allCurrency").style.display="none";
    document.getElementById("allBank").style.display="none";
    document.getElementById("revHistory").style.display="none";
}

function registerCurrency(){
   var currencyName= document.getElementById("currencyName").value;
   var currencyRate= document.getElementById("rate").value;
   var currencyId= document.getElementById("currencyId").value;
   var excFees1= document.getElementById("excFees1").value;
   var currency={
     CurrencyId:currencyId,
     CurrencyName:currencyName,
     CurrencyRate:currencyRate,
     ExchangeFees:excFees1
    
   }
   var excRate=localStorage.getItem("excRate");
   excRate=JSON.parse(excRate);

   if(excRate == null){  
    excRate=[];     
   }
   
   excRate.push(currency);
   excRate=JSON.stringify(excRate);
   localStorage.setItem("excRate",excRate);
   
}

function allBanks(){

    document.getElementById("registerBank").style.display="none";
    document.getElementById("registerCurrency").style.display="none";
    document.getElementById("allCurrency").style.display="none";
    document.getElementById("allBank").style.display="block";
    document.getElementById("revHistory").style.display="none";


     var  allBank=fetchAllBanks();
    for(var i=0;i<allBank.length;i++){
        console.log(allBank[i]);
    }

    const table = document.createElement('table');
    table.className="myTable";
    const thead = document.createElement('thead');
    const tbody = document.createElement('tbody');
  
    // Create table header
    const headerRow = document.createElement('tr');
    for (const key of Object.keys(allBank[0].Record)) {
      if(key == "amount" || key == "assetType"){
       continue; 
      
      }else{
        const th = document.createElement('th');
        th.textContent = key;
        headerRow.appendChild(th);
      }
    }
    thead.appendChild(headerRow);
    table.appendChild(thead);
  
    // Create table rows

    allBank.forEach(item => {
    
        const row = document.createElement('tr');
        Object.keys(item.Record).forEach(key => {
          if (key !== 'amount' && key !== 'assetType') {
            const cell = row.insertCell();
            const text = document.createTextNode(item.Record[key]);
            cell.appendChild(text);
          }
        });
        tbody.appendChild(row);
      });
 
    table.appendChild(tbody);


  // Add the table to the HTML body
  const tableContainer = document.getElementById('allBank');
  tableContainer.innerHTML="";
  tableContainer.appendChild(table);
  
}

function allCurrency(){

    document.getElementById("registerBank").style.display="none";
    document.getElementById("registerCurrency").style.display="none";
    document.getElementById("allCurrency").style.display="block";
    document.getElementById("allBank").style.display="none";
    document.getElementById("revHistory").style.display="none";


    var allCurrencies=localStorage.getItem("excRate");
    allCurrencies= JSON.parse(allCurrencies);

    const table = document.createElement("table");
    table.className="myTable";

// Create the table header
const thead = document.createElement("thead");
const headerRow = document.createElement("tr");
const headers = Object.keys(allCurrencies[0]); // Assuming all objects have the same keys

headers.forEach(headerText => {
  const th = document.createElement("th");
  th.appendChild(document.createTextNode(headerText));
  headerRow.appendChild(th);
});

thead.appendChild(headerRow);
table.appendChild(thead);

// Create the table body
const tbody = document.createElement("tbody");

allCurrencies.forEach(item => {
  const row = document.createElement("tr");

  Object.values(item).forEach(value => {
    const td = document.createElement("td");
    td.appendChild(document.createTextNode(value));
    row.appendChild(td);
  });

  tbody.appendChild(row);
});

table.appendChild(tbody);

// Append the table to a container in your HTML
const container = document.getElementById("allCurrency");
container.innerHTML="";
container.appendChild(table);

}


function disRevHis(){

    document.getElementById("registerBank").style.display="none";
    document.getElementById("registerCurrency").style.display="none";
    document.getElementById("allCurrency").style.display="none";
    document.getElementById("allBank").style.display="none";
    document.getElementById("revHistory").style.display="block";


  var data=fetchRevHistory();

  const table = document.createElement("table");
  table.className="myTable";

// Create the table header
const thead = document.createElement("thead");
const headerRow = document.createElement("tr");
Object.keys(data[0]).forEach(key => {
  const th = document.createElement("th");
  th.textContent = key;
  headerRow.appendChild(th);
});
thead.appendChild(headerRow);
table.appendChild(thead);

// Create the table body
const tbody = document.createElement("tbody");
data.forEach(item => {
  const row = document.createElement("tr");
  Object.values(item).forEach(value => {
    const td = document.createElement("td");
    td.textContent = typeof value === "object" ? JSON.stringify(value) : value;
    row.appendChild(td);
  });
  tbody.appendChild(row);
});
table.appendChild(tbody);

// Append the table to the document body
document.getElementById("revHistory").innerHTML="";
document.getElementById("revHistory").appendChild(table);
}