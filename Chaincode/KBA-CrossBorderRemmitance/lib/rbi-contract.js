/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class RbiContract extends Contract {

    async rbiExists(ctx, rbiId) {
        const buffer = await ctx.stub.getState(rbiId);
        return (!!buffer && buffer.length > 0);
    }

    async createRbi(ctx, rbiId ) {
        const exists = await this.rbiExists(ctx, rbiId);
        if (exists) {
            throw new Error(`The rbi ${rbiId} already exists`);
        }
        const asset = { 
            revenue :0,
            issuerbank : "",
            acquierbank :"" , 
            fees :0
         };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(rbiId, buffer);
    }

    async readRbi(ctx, rbiId) {
        const exists = await this.rbiExists(ctx, rbiId);
        if (!exists) {
            throw new Error(`The rbi ${rbiId} does not exist`);
        }
        const buffer = await ctx.stub.getState(rbiId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateRbi(ctx, rbiId, newValue) {
        const exists = await this.rbiExists(ctx, rbiId);
        if (!exists) {
            throw new Error(`The rbi ${rbiId} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(rbiId, buffer);
    }

    async deleteRbi(ctx, rbiId) {
        const exists = await this.rbiExists(ctx, rbiId);
        if (!exists) {
            throw new Error(`The rbi ${rbiId} does not exist`);
        }
        await ctx.stub.deleteState(rbiId);
    }

    async getAllResults(iterator, isHistory) {
        let allResult = []
        for (let res = await iterator.next(); !res.done; res = await iterator.next()) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {}
                if (isHistory && isHistory == true) {
                    jsonRes.TxId = res.value.txId
                    jsonRes.TimeStamp = res.value.timestamp
                    jsonRes.Record = JSON.parse(res.value.value.toString())

                }
                else {
                    jsonRes.Key = res.value.key
                    jsonRes.Record = JSON.parse(res.value.value.toString())
                }
                allResult.push(jsonRes)

            }
        }
        await iterator.close()
        return allResult
    }


    async getRbiHistory(ctx, rbiId) {
        let resultIterator = await ctx.stub.getHistoryForKey(rbiId)
        let result = await this.getAllResults(resultIterator, true)
        return JSON.stringify(result)
    }


    async addRevenue(ctx, rbiId,issuerbank, acquierbank , fees ) {
        const exists = await this.rbiExists(ctx, rbiId);
        if (!exists) {
            throw new Error(`The rbi ${rbiId} does not exist`);
        }

        const asset = await this.readRbi(ctx, rbiId);

        asset.revenue =parseInt(asset.revenue)+parseInt(fees);
        asset.issuerbank =issuerbank;
        asset.acquierbank=acquierbank;
        asset.fees= fees

        const buffer = Buffer.from(JSON.stringify(asset));
        let addrevenueEventData = { Type: 'Revenue added', revenue: asset.revenue };
        await ctx.stub.setEvent('addRevenue', Buffer.from(JSON.stringify(addrevenueEventData)));
        await ctx.stub.putState(rbiId, buffer);
    }

}

module.exports = RbiContract;
