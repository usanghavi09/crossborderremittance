var express = require('express');
var router = express.Router();
const { ClientApplication } = require('../../Client/client')
let RbiClient = new ClientApplication();


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'KBA-CrossBorderRemmitance', dashboard: 'Rbi Dashboard' });
});

router.get('/login', function (req, res, next) {
  res.render('login', { title: 'KBA-CrossBorderRemmitance', dashboard: 'Rbi Dashboard' });
});

router.get('/rbi', function (req, res, next) {
  res.render('rbi', { title: 'KBA-CrossBorderRemmitance', dashboard: 'Rbi Dashboard' });
})

router.get('/bank', function (req, res, next) {
  res.render('bank', { title: 'KBA-CrossBorderRemmitance', dashboard: 'Rbi Dashboard' });
})



router.post('/createBank', function (req, res) {

  const bankId = req.body.BankId;
  const BankName = req.body.BankName;  
  const BankCountry = req.body.BankCountry;
  const BankCurrency = req.body.BankCurrency;
  const amount = req.body.Amount;

  RbiClient.generateAndSubmitTxn(
    "rbi",
    "Admin",
    "cbrchannel",
    "KBA-CrossBorderRemmitance",
    "BankContract",
    "invokeTxn",
    "",
    "createBank",
    bankId, BankName,BankCountry,BankCurrency,amount
  )
    .then(message => {
      console.log("Message is ", message)
      res.status(200).send({ message: "Added Bank" })
    }
    )
    .catch(error => {
      console.log("Some error Occured: ", error)
      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });
});

router.post('/readBank', async function (req, res) {
  const readBankId = req.body.BankId;

  RbiClient.generateAndSubmitTxn(
    "rbi",
    "Admin",
    "cbrchannel",
    "KBA-CrossBorderRemmitance",
    "BankContract",
    "queryTxn",
    "",
    "readBank",
    readBankId
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Bankdata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})

router.post('/allBank', async function (req, res) {

  RbiClient.generateAndSubmitTxn(
    "rbi",
    "Admin",
    "cbrchannel",
    "KBA-CrossBorderRemmitance",
    "BankContract",
    "queryTxn",
    "",
    "queryAllBanks",
  
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Bankdata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})


router.post('/updateAmount', function (req, res) {

  const issuerBankId = req.body.issuerBankId;
  const acquirerBankId = req.body.acquirerBankId;
  const Fees = req.body.Fees;  
  const TxnType = req.body.TxnType;
  const amount = req.body.amount;
  const excRate=req.body.excRate;

  RbiClient.generateAndSubmitTxn(
    "rbi",
    "Admin",
    "cbrchannel",
    "KBA-CrossBorderRemmitance",
    "BankContract",
    "invokeTxn",
    "",
    "updateAmount",
    issuerBankId,acquirerBankId, Fees,TxnType,amount,excRate
  )
    .then(message => {
      console.log("Message is ", message)
      res.status(200).send({ message: "Added Bank" })
    }
    )
    .catch(error => {
      console.log("Some error Occured: ", error)
      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });
});

router.post('/bankHistory', async function (req, res) {
  const readBankId = req.body.BankId;

  RbiClient.generateAndSubmitTxn(
    "rbi",
    "Admin",
    "cbrchannel",
    "KBA-CrossBorderRemmitance",
    "BankContract",
    "queryTxn",
    "",
    "getBankHistory",
    readBankId
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Bankdata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})

router.post('/addRevenu', function (req, res) {

  const issuerBankId = req.body.issuerBankId;
  const acquirerBankId = req.body.acquirerBankId;
  const Fees = req.body.Fees;  


  RbiClient.generateAndSubmitTxn(
    "rbi",
    "Admin",
    "cbrchannel",
    "KBA-CrossBorderRemmitance",
    "RbiContract",
    "invokeTxn",
    "",
    "addRevenue",
     "R1",issuerBankId,acquirerBankId, Fees,
  )
    .then(message => {
      console.log("Message is ", message)
      res.status(200).send({ message: "Added Bank" })
    }
    )
    .catch(error => {
      console.log("Some error Occured: ", error)
      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });
});


router.post('/revHistory', async function (req, res) {
 

  RbiClient.generateAndSubmitTxn(
    "rbi",
    "Admin",
    "cbrchannel",
    "KBA-CrossBorderRemmitance",
    "RbiContract",
    "queryTxn",
    "",
    "getRbiHistory",

    "R1"
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ Bankdata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})

module.exports = router;
