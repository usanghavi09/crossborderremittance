/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const RbiContract = require('./lib/rbi-contract');
const BankContract=require('./lib/bank-contract');
const ExchangeContract =require('./lib/exchange-contract');

module.exports.RbiContract = RbiContract;
module.exports.BankContract = BankContract;
module.exports.ExchangeContract = ExchangeContract;
module.exports.contracts = [ RbiContract ,BankContract,ExchangeContract];
