/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class BankContract extends Contract {

    async bankExists(ctx, bankId) {
        const buffer = await ctx.stub.getState(bankId);
        return (!!buffer && buffer.length > 0);
    }

    async createBank(ctx, bankId, bankName ,counntry,currency ,amount) {
        const exists = await this.bankExists(ctx, bankId);
        if (exists) {
            throw new Error(`The bank ${bankId} already exists`);
        }
        const asset = { 
            assetType: 'bank',
            bankId:bankId,
            bankName :bankName,
            counntry:counntry,
            currency:currency,
            amount:amount,
             
        };
        const buffer = Buffer.from(JSON.stringify(asset));

        let addBankData = { Type: 'New Bank added', BankName: bankName };
        await ctx.stub.setEvent('addBank', Buffer.from(JSON.stringify(addBankData)));

        await ctx.stub.putState(bankId, buffer);
    }

    async readBank(ctx, bankId) {
        const exists = await this.bankExists(ctx, bankId);
        if (!exists) {
            throw new Error(`The bank ${bankId} does not exist`);
        }
        const buffer = await ctx.stub.getState(bankId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateBank(ctx, bankId, newValue) {
        const exists = await this.bankExists(ctx, bankId);
        if (!exists) {
            throw new Error(`The bank ${bankId} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(bankId, buffer);
    }

    async deleteBank(ctx, bankId) {
        const exists = await this.bankExists(ctx, bankId);
        if (!exists) {
            throw new Error(`The bank ${bankId} does not exist`);
        }
        await ctx.stub.deleteState(bankId);
    }

    async addAmount(ctx, bankId, amount ) {
        const exists = await this.bankExists(ctx, bankId);
        if (!exists) {
            throw new Error(`The bank ${bankId} does not exist`);
        }
        amount=parseInt(amount);
        
        const asset = await this.readBank(ctx, bankId);
       
        asset.amount =parseInt(asset.amount)+amount;

        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(bankId, buffer);
    }

    // async updateAmount(ctx, bankId,fees,txnType, amount ) {
    //     const exists = await this.bankExists(ctx, bankId);
    //     if (!exists) {
    //         throw new Error(`The bank ${bankId} does not exist`);
    //     }

    //     amount=parseInt(amount);
        
    //     const asset = await this.readBank(ctx, bankId);
     
    //     if(txnType =="Debit"){
    //         fees =parseInt(fees);
    //         if(parseInt(asset.amount) < fees){
    //             alert("Not Sufficient Amount");
    //             return;
    //         }
    //         if(parseInt(asset.amount) < amount ){
    //             alert("Not Sufficient Amount");
    //             return;
    //         }else{
    //             asset.amount =parseInt(asset.amount)-amount;
    //         }
    //     }else{
    //         asset.amount =parseInt(asset.amount)+amount;
    //     }

    //     const buffer = Buffer.from(JSON.stringify(asset));
    //     await ctx.stub.putState(bankId, buffer);
    // }

    async updateAmount(ctx, issuerbankId,acquierbankId,fees,txnType, amount ,excRate) {
        const exists = await this.bankExists(ctx, issuerbankId);
        if (!exists) {
            throw new Error(`The bank ${issuerbankId} does not exist`);
        }
        

        amount=parseInt(amount);
        
        const issuerbank = await this.readBank(ctx, issuerbankId);
     
        if(txnType =="Debit"){

           var exists1 = await this.bankExists(ctx, acquierbankId);
            if (!exists1) {
                throw new Error(`The bank ${acquierbankId} does not exist`);
            }
            const acquierbank = await this.readBank(ctx, acquierbankId);

            if((issuerbank.currency == "INR" && acquierbank.currency != "INR") || (issuerbank.currency != "INR" && acquierbank.currency == "INR")){
              
                fees =parseInt(fees);
                excRate=parseInt(excRate);
                var dbtAmt;
                var cdtAmt;

                if(issuerbank.currency != "INR"){
                    dbtAmt=amount;
                    cdtAmt=(amount*excRate)-fees;
                }else{
                    dbtAmt=amount;
                    cdtAmt=(amount-fees)/excRate;
                }

            
            if(parseInt(issuerbank.amount) < fees){
                alert("Not Sufficient Amount");
                return;
            }else if(parseInt(issuerbank.amount) < amount ){
                alert("Not Sufficient Amount");
                return;
            }else{
                issuerbank.amount =parseInt(issuerbank.amount)-dbtAmt;
                acquierbank.amount =parseInt(acquierbank.amount)+cdtAmt;
                const buffer1 = Buffer.from(JSON.stringify(acquierbank));
                 await ctx.stub.putState(acquierbankId, buffer1);
            }}else{
               alert("Money can be exchange between INR anf foreign currency");
            }
        }else{
            issuerbank.amount =parseInt(issuerbank.amount)+amount;
        }

        const buffer = Buffer.from(JSON.stringify(issuerbank));
        await ctx.stub.putState(issuerbankId, buffer);
        
    }

    async getAllResults(iterator, isHistory) {
        let allResult = []
        for (let res = await iterator.next(); !res.done; res = await iterator.next()) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {}
                if (isHistory && isHistory == true) {
                    jsonRes.TxId = res.value.txId
                    jsonRes.TimeStamp = res.value.timestamp
                    jsonRes.Record = JSON.parse(res.value.value.toString())

                }
                else {
                    jsonRes.Key = res.value.key
                    jsonRes.Record = JSON.parse(res.value.value.toString())
                }
                allResult.push(jsonRes)

            }
        }
        await iterator.close()
        return allResult
    }


    async queryAllBanks(ctx) {
        const queryString = {
            selector: {
                assetType: 'bank'
            },
            
        }
        let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString))
        let result = await this.getAllResults(resultIterator, false)
        return JSON.stringify(result)
    }

    async getBankHistory(ctx, BankId) {
        let resultIterator = await ctx.stub.getHistoryForKey(BankId)
        let result = await this.getAllResults(resultIterator, true)
        return JSON.stringify(result)
    }



}

module.exports = BankContract;
